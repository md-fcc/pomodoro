My pomodoro clock to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/build-a-pomodoro-clock).

Live website: [Pomodoro](https://md-fcc.gitlab.io/pomodoro/)

The Javascript source code is split into four modules:
- main.js - Entry point of the program, defines initial state of the clock, HTML click handlers, etc
- pubsub.js - I wanted to try the publish-subscribe design pattern to decople code. This is a simple implementation that helped me learn.
- pomodoro_clock.js - Contains code defining the clock time, status, and publishes ("emits") changes through pubsub
- analog_clock_render.js - Contains code to display the clock status in HTML elements. I wanted to use an analog clock renderer to learn about the HTML5 canvas object. The clock render could be easily swapped-out for a different implementation because the pubsub decouples the clock logic from the rendering.

I learned about Webpack and Babel tools while building this game. Not a lot of browsers support the ES6 module import/export features, so I am using these tools to transpile the code.
