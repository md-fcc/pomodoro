export class analogRender {
    
    constructor(canvas, eventBus) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');

        this.clockRadius = 100;
        this.handLength = 100;
        this.tickLength = this.handLength / 10;

        this.faceCenterX = this.canvas.width / 2.0;
        this.faceCenterY = this.canvas.height / 2.0;

        this.eventBus = eventBus;
    }
    
    draw_clock_face(clock) {
        // draws the clock circle and other elements of the clock.

        // draw the work & break time wedges
        this.ctx.beginPath();
        this.ctx.fillStyle = '#D3BDE0';
        this.ctx.moveTo(this.faceCenterX, this.faceCenterY);
        let startAngle = 0 - Math.PI / 2.0;
        let endAngle = clock.workTime_m * 60 * (360 / clock.max_sec) * Math.PI / 180.0 - Math.PI / 2.0;
        this.ctx.arc(this.faceCenterX, this.faceCenterY, this.clockRadius, startAngle, endAngle);
        this.ctx.closePath();
        this.ctx.fill();

        // Draw the clock circle
        this.ctx.beginPath();
        this.ctx.arc(this.faceCenterX, this.faceCenterY, this.clockRadius, 0, Math.PI * 2);
        this.ctx.strokeStyle = '#000000';
        this.ctx.stroke();    
  
        // Draw the ticks
        let ticks = clock.max_sec / 60;
        for (let i = 0; i < ticks; i++) {    
            
            this.ctx.lineWidth = 1;
            this.ctx.beginPath();
            
            let angle = i * (360.0 / ticks) * Math.PI / 180.0 - Math.PI / 2.0;
            let x1 = this.faceCenterX + Math.cos(angle) * (this.handLength);
            let y1 = this.faceCenterY + Math.sin(angle) * (this.handLength);
            let x2 = this.faceCenterX + Math.cos(angle) * (this.handLength - this.tickLength);
            let y2 = this.faceCenterY + Math.sin(angle) * (this.handLength - this.tickLength);
        
            this.ctx.moveTo(x1, y1);
            this.ctx.lineTo(x2, y2);
            
            this.ctx.strokeStyle = '#000000';
            this.ctx.stroke();
        }
    }

    draw_hand(clock) {
        // draws the clock hand that points to the current time
        this.ctx.beginPath();
        this.ctx.moveTo(this.faceCenterX, this.faceCenterY);

        let angle = clock.elapsed_sec * (360 / clock.max_sec) * Math.PI / 180.0 - Math.PI / 2.0;
        this.ctx.lineTo(this.faceCenterX + Math.cos(angle) * this.handLength, 
                        this.faceCenterY + Math.sin(angle) * this.handLength);
        
        this.ctx.strokeStyle = '#000000';
        this.ctx.stroke();
    }
    
    render(clock) {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height); 
        this.draw_clock_face(clock);
        this.draw_hand(clock);
    }
}
