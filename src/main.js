import {PubSub} from './pubsub.js';
import {PomodoroClock} from './pomodoro_clock.js';
import {analogRender} from './analog_clock_render.js';

// Use pub/sub pattern to handle communication b/n objects
// http://dev.housetrip.com/2014/09/15/decoupling-javascript-apps-using-pub-sub-pattern/
$(document).ready(function () {
    let pubsub = new PubSub();
    let pomodoroClock = new PomodoroClock(pubsub);
    let renderer = new analogRender(document.getElementById('canvas'), pubsub);

    pubsub.subscribe('clock_changed', () => renderer.render(pomodoroClock));
    pubsub.subscribe('clock_status_change', show_clock_status);
    pubsub.subscribe('completed_pomodoro', show_completed_pomodoros);

    // show initial state of clock
    renderer.render(pomodoroClock);
    show_clock_status(pomodoroClock.status);
    show_completed_pomodoros(pomodoroClock.completed_pomodoros);


    // Functions for HTML Buttons ----------------------------------------------
    $('#work-time').on("change", 
        (e) => {
            let time = e.target.value;
            // limit value to bounds
            if (time < 5) {
                time = 5;
                e.target.value = time;
            }
            if (time > 60) {
                time = 60;
                e.target.value = time;
            }
            pomodoroClock.set_work_time(parseInt(time));
        });

    $('#break-time').on("change", 
        (e) => {
            let time = e.target.value;
            // limit value to bounds
            if (time < 1) {
                time = 1;
                e.target.value = time;
            }
            if (time > 10) {
                time = 10;
                e.target.value = time;
            }
            pomodoroClock.set_break_time(parseInt(time));
        });

    $("#btn-begin").on("click", (e) => {
        pomodoroClock.begin();
        toggleHTMLcontrols();
    });

    $("#btn-reset").on("click", (e) => {
        pomodoroClock.reset();
        toggleHTMLcontrols();
    });

    function toggleHTMLcontrols() {
        // toggles the state of the disabled property on HTML controls
        $('#work-time').prop('disabled',  (i, v) => { return !v; });
        $('#break-time').prop('disabled', (i, v) => { return !v; });
        $('#btn-begin').prop('disabled',  (i, v) => { return !v; });
        $('#btn-reset').prop('disabled',  (i, v) => { return !v; });
    }

    function show_clock_status(status) {
        $("#status-label").text(status);
    }

    function show_completed_pomodoros(count) {
        $("#completed-pomodoros").text(count);
    }
});
