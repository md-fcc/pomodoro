export class PomodoroClock {
    constructor(eventBus) {
        // workTime & breakTime are in units of minutes "_m"
        this.workTime_m = 25; // default
        this.breakTime_m = 5; // default       
        
        this.elapsed_sec = 0;
        this.max_sec = this.calc_max_sec();
        
        this.timerID = null; // used to track the setInterval function called in this.begin()

        this.completed_pomodoros = 0;
        this.status = "STOPPED";

        this.eventBus = eventBus; // pub/sub communication channel
    }

    tick() {
        // increment the clock time

        this.elapsed_sec++;
        if (this.elapsed_sec >= this.max_sec) {
            this.elapsed_sec = 0;
            this.completed_pomodoros += 1;
            this.eventBus.publish('completed_pomodoro', this.completed_pomodoros);
        }
        
        let oldStatus = this.status;
        if (this.elapsed_sec >= this.workTime_m * 60) {
            this.status = "BREAK TIME!";    
        } else {
            this.status = "WORK TIME";
        }
        if (oldStatus != this.status) {
            this.eventBus.publish('clock_status_change', this.status);
        }

        this.eventBus.publish('clock_changed', this);
    }
    
    set_work_time(time) {
        if (this.elapsed_sec > 0) return; // can't change time while clock is running.
        this.workTime_m = time;
        this.calc_max_sec();
        this.eventBus.publish('clock_changed', this);
    }

    set_break_time(time) {
        if (this.elapsed_sec > 0) return; // can't change time while clock is running.
        this.breakTime_m = time;
        this.calc_max_sec();
        this.eventBus.publish('clock_changed', this);
    }

    calc_max_sec() {
        return this.max_sec = (this.workTime_m + this.breakTime_m) * 60;
    }
    
    set_render(render_callback) {
        this.render = render_callback
    }
    
    begin() {
        this.timerID = setInterval(this.tick.bind(this), 1000);
        this.status = "WORK TIME";
        this.eventBus.publish('clock_status_change', this.status);
    }

    reset() {
        if (this.timerID) {
            clearInterval(this.timerID);
            this.elapsed_sec = 0;
            this.completed_pomodoros = 0;
            this.status = "STOPPED";
            this.eventBus.publish('clock_changed', this);
            this.eventBus.publish('clock_status_change', this.status);
            this.eventBus.publish('completed_pomodoro', this.completed_pomodoros);
        }
    }
}
