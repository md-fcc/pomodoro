export class PubSub {
    // modeled after:
    // https://gist.github.com/learncodeacademy/777349747d8382bfb722
    // also
    // https://gist.github.com/fatihacet/1290216

    constructor() {
        this.topics = {};
    }

    subscribe(topic, callback) {
        this.topics[topic] = this.topics[topic] || [];
        this.topics[topic].push({callback: callback});
    }

    // unsubscribe(topic, object, callback) {
    // /* NOT IMPLEMENTED */
    //     if (!this.topics[topic]) return;
    //     for (let i = 0; i < this.topics[topic].length; i++) {
    //         if (this.topics[topic][i].object === object) {
    //             this.topics[topic].splice(i, 1);
    //             break;
    //         }
    //     }
    // }

    publish(topic, data) {
        if (!this.topics[topic]) return;

        this.topics[topic].forEach( (i) => {i.callback(data)});
    }
}
